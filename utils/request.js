import { baseUrl } from "./config.js";
// import Dialog from '@vant/weapp/dist/dialog/dialog';

/**
 * 封装请求
 * url：请求地址
 * data：请求参数
 * method: 请求类型
 */

const request = (url, data, method) => {
  // 获取token,登录时存的
  let token = wx.getStorageSync("token");
  url = baseUrl + url;
  return new Promise((resolve, reject) => {
    // 请求
    wx.request({
      url,
      method,
      data,
      // 把token加入到请求头
      header: {
        "user-token": token,
      },
      success: (res) => { 
        if (res.data.code == 666) {
          resolve(res.data);
        } else if (res.data.code == 603) {
          wx.removeStorageSync("token");
          wx.showModal({
            title: "提示",
            content: "登录已过期，是否重新登录",
            success(res) {
              if (res.confirm) {
                // 跳转到个人中心页面
                wx.switchTab({
                  url: "/pages/my/my",
                });
              } else if (res.cancel) {
                console.info("用户点击取消");
              }
            },
          });
        } else {
          // wx.dialog(res.data.msg);
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 1500
          })

          // Dialog.alert({
          //   message: res.data.msg,
          // }).then(() => {
          //   // on close
          // })
        }
      },
      fail: (err) => { 
        wx.showToast({
          title: '网络异常',
          icon: 'none',
          duration: 1500
        })
      },
    });
  });
};

const get = (url, data) => {
  return request(url, data, "get");
};

const post = (url, data) => {
  return request(url, data, "post");
};

export default {
  get,
  post,
};