// config.js
let env = "prod";
let baseUrl = "";
if (env === "dev") {
  // 本地地址
  baseUrl = "http://localhost:3008";
} else if(env === 'prod'){
  baseUrl = "http://huruqing.cn:3008";
}
// 导出
export {
  baseUrl
};