// app.js
import request from './utils/request';
App({
  onLaunch() {  
    this.$get = request.get;
    this.$post = request.post;
  },
  globalData: {
    // 当前城市名称
    cityName:'深圳',
    msg: 'hello mini',
    userInfo: null
  }
})
