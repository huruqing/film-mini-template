// pages/cinema/cinema.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 是否显示空信息
    show: false,
    cityName: '',
    // 影院列表
    cinemas: [],
    // 影院列表(全部数据,筛选时不修改)
    all: [],
    // 区域列表
    areas: [{
      text: '全城',
      value: 0
    }],
    area: 0,

    option2: [{
      text: '品牌',
      value: 0
    }],
    option3: [{
      text: '特色',
      value: 0
    }],
    value2: 0,
    value3: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 判断是否有城市名称, 没有就跳转到城市列表去选择城市
    let cityName = app.globalData.cityName
    if (!cityName) {
      wx.navigateTo({
        url: '/pages/city/city',
      })
    } else {
      this.setData({
        cityName
      })
      // 根据城市名称获取影院列表
      this.getCinemas();
      // 根据城市名称获取其区域列表
      this.getAreas();
    }
  },

  onHide() {
    this.setData({
      show: false
    })
  },

  // 选择区域,value为区域的id
  selectArea(value) {
    // 获取区域id
    value = value.detail;
    // id为0时为全称
    if (value === 0) {
      this.setData({
        cinemas: this.data.all
      })
    } else {
      // 筛选出areaId等于value的影院
      let list = this.data.all.filter(item => item.areaId === value);
      this.setData({
        cinemas: list
      })
    } 
  },

  // 根据城市名称获取其区域列表
  getAreas() {
    let data = {
      cityName: this.data.cityName
    }
    wx.showLoading({
      title: '努力加载中...',
    })
    app.$get('/area/list', data).then(res => {
      // 将区域列表改造成下拉组件需要的数据结构
      // [{id:'xxx',name:'xxx'},id:'xxx',name:'xxx'}]
      // [{text:'xxx',value:'xx'},{text:'xxx',value:'xx'}]
      let areas = res.data.map(item => {
        return {
          text: item.name,
          value: item.id
        }
      })
      // 添加全称
      areas.unshift({
        text: '全城',
        value: 0
      })
      this.setData({
        areas: areas
      }, () => {
        wx.hideLoading();
      })
    })
  },

  // 根据城市名称获取影院列表
  getCinemas() {
    let data = {
      cityName: this.data.cityName
    }
    wx.showLoading({
      title: '努力加载中...',
    })
    app.$get('/cinema/list', data).then(res => {  
      this.setData({
        show: res.data.length === 0,
        cinemas: res.data,
        all: res.data,
      }, () => {
        wx.hideLoading();
      })
    })
  }
})