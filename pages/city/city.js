// pages/city/city.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 当前城市名称
    cityName:'定位中...',
    // 城市列表
    citys: [],
    hotCitys: [],

  },

  onLoad() {
    this.getCitys();
  },

  // 获取城市列表
  getCitys() {
    wx.showLoading({
      title: '正在努力加载中...',
    })
    app.$get('/city/list').then(res => {
      // 过滤出热门城市
      let hotCitys = res.data.filter(item => item.isHot === 1);
      this.setData({
        hotCitys,
        citys: res.data
      },()=> {
        wx.hideLoading();
      }) 
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getCurrCity();
  },

  // 选择城市
  selectCity(event) {
    // 获取自定义属性data-cityName
    let cityName =event.currentTarget.dataset.cityname;
    // 将选中的城市赋值到全局变量app.globalData.cityName
    app.globalData.cityName = cityName;
    // 跳转到影院列表
    wx.switchTab({
      url: '/pages/cinema/cinema',
    })
  },

  // 获取当前城市
  getCurrCity() {
    wx.getLocation({
      type: 'wgs84',
      success: (res) => {
        const latitude = res.latitude;
        const longitude = res.longitude;
        wx.request({
          url: `https://apis.map.qq.com/ws/geocoder/v1/?location=${latitude},${longitude}&key=XBZBZ-OBG63-LOD3N-3QR5Q-X6Z2Q-BFBIR`,
          success: (res) => { 
            let cityName = res.data.result.address_component.city; 
            this.setData({
              cityName: cityName.slice(0,-1)
            })
          }
        }) 
      },
      fail: (err) => {
        this.setData({
          city: '定位失败,请手动选择'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})