Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 2,
    list:['a','b','c','d']
  },
 

  onChange(tab) {
    console.log(tab.detail.name);
    if (tab.detail.name === 'aaa') {
      this.setData({
        list: [1,2,3,4,5]
      })
    } else if (tab.detail.name === 'bbb') {
      this.setData({
        list: [10,20,30,40,50]
      })
    } else {
      this.setData({
        list: ['a','b','c','d']
      })
    }

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})