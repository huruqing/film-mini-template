// pages/show/show.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 左列
     leftList:[],
    // 右列
     rightList:[],
  },
  
  onLoad() {
    this.getList();
  },

  // 获取演出列表
  getList() {
    wx.showLoading({
      title: '努力加载中...',
    })
    app.$get('/show/list').then(res=> {
      let list = res.data;
      let leftList = [];
      let rightList = [];

      list.forEach((item,index)=> {
        if (index%2 === 0) {
          leftList.push(list[index]);
        } else {
          rightList.push(list[index]);
        } 
        this.setData({
          leftList,
          rightList
        },()=> {
          wx.hideLoading();
        });
      }) 
    })
  }
})