// pages/video/list.js
const app =getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    // 视频列表
    videoList: []    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getVideoList();
  }, 

  getVideoList() {
    app.$get('/video/list').then(res=> {
      this.setData({
        videoList: res.data
      })
    })
  }
})