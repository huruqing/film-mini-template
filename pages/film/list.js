// pages/film/list.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // tab高亮小标
    active:0,
    // 电影类型 1-热映,2-待映,3-经典
    type:1,
    // 轮播图列表
    banners:[], 
    // 影片列表
    films:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.getBanners();
      this.getFilms();
  },

  // 切换tab栏
  changeTab(ev) {
    let name = ev.detail.name; 
    console.log(name);
    // 修改type
    this.setData({
      type: name
    })
    this.getFilms();
  },

  // 获取电影列表
  getFilms() {
    let data = {
      type: this.data.type
    } 
    wx.showLoading({
      title: '努力加载中...',
    })
    app.$get('/film/list',data).then(res=> { 
      // 修改films的值
      this.setData({
        films: res.data
      })
      wx.hideLoading();
    })
  },

  // 获取轮播图列表
  getBanners() {
    app.$get('/film/banners').then(res=> { 
      // 修改banners的数据
      this.setData({
        banners: res.data 
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})