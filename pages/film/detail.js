const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 电影详情对象
    film: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getFilm(options.id);
  },

  getFilm(id) {
    app.$get('/film/detail', {
      id
    }).then(res => {
      let data = res.data;

      this.setData({
        film: {
          ...data,
          premiereAt: this.fTime(data.premiereAt), 
        }
      })
    })
  },

  // 格式化首映式
  fTime(time) {
    let date = new Date(time);
    return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
  } 
})