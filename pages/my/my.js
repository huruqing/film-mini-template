const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let token = wx.getStorageSync('token');
    this.setData({
      token
    });
  },

  // 获取登录接口所需的code
  getCode() {
    return new Promise((resolve, reject) => {
      wx.login({
        success(res) {
          // 把code保存到promiseObj
          resolve(res.code);
        },
        fail(err) {
          reject(err);
        }
      })
    }) 
  },


  // 立即登录
  loginNow(res) {
    let iv;
    let encryptedData;
    let userInfo;
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: async (res) => {
        console.log(res);
        iv = res.iv;
        encryptedData = res.encryptedData;
        userInfo = res.userInfo;

        // 获取code
        let code = await this.getCode();
        let data = {
          code,
          iv,
          encryptedData,
          userInfo,
        }
        // console.log('data',data);
        app.$get('/user/login', data).then(res=> {
          this.setData({
            token: res.token
          })
          // 把token保存到缓存
          wx.setStorageSync('token',  res.token);
        });
      }
    }) 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})